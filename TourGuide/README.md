# TourGuide

This app is used to get the tourist attractions near the user and collect rewards points.
This app uses JAva to run

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Java 1.8
- Gradle 4.8.1

### Installing

A step by step series of examples that tell you how to get a development env running:

1. Install Java

https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html

2. Install Gradle

https://gradle.org/install/

### Testing

The app has unit tests and integration tests written.
To run the tests from gradle, go to the folder that contains the build.gradle file and execute the below command.

`./gradlew test`